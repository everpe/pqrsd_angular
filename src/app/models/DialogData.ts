import { Tercero } from './Tercero';

// Se usa para el mensaje del dialogo al registrar tercero.
export interface DialogData {
    message: string;
    tercero:Tercero
  }
  