import { TipoDocumento } from './TipoDocumento';

export class Pqrsd {

    public  codigo_oficio_recibido?: any; //ES Opcional Al Recibirlo y ni lo declaro en 
                                //Constructor para no tener que enviarlo al crear un PQrsd
    public  numero_radicado?: string;     //NO SE ENVIA AL CREAR PERO SE RECIBE EN RESPONSE
    public  fecha_oficio?:any;
    public  descripcion_asunto:string;
    public  fecha_radicado?:any;
    public  fecha_limite_respuesta?:any; //NO SE ENVIA AL CREAR PERO SE RECIBE EN RESPONSE  
    public  estado_documento:boolean;
    public  estado_contestado:boolean;
    public  autorizacion_correo:boolean;
    public  correo_envio?:string; 
    public  codigo_qr?:string            //NO SE ENVIA AL CREAR PERO SE RECIBE EN RESPONSE
    //ForaneasNecesarias
    public tipoDocumento?:TipoDocumento;


    constructor(
        // numero_radicado: string,
        fecha_oficio:any,
        descripcion_asunto:string,
        fecha_radicado:any, 
        // fecha_limite_respuesta:any, 
        estado_documento:boolean,
        estado_contestado:boolean,
        autorizacion_correo:boolean,
        correo_envio:string,
        // codigo_qr:string, 
        tipoDocumento:TipoDocumento,
    ){
        // this.numero_radicado=numero_radicado;
        this.fecha_oficio=fecha_oficio;
        this.descripcion_asunto=descripcion_asunto;
        this.fecha_radicado=fecha_radicado;
        // this.fecha_limite_respuesta=fecha_limite_respuesta;
        this.estado_documento=estado_documento;
        this.estado_contestado=estado_contestado;
        this.autorizacion_correo=autorizacion_correo;
        this.correo_envio=correo_envio;
        // this.codigo_qr=codigo_qr;
        this.tipoDocumento=tipoDocumento;
    }

}