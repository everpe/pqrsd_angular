// Representa la entidad detalle_tipo_documento
export class TipoDocumento {

    public codigo_documento?:string;
    public fk_codigo_tipo_pqrsd:string;
    public nombre_documento:string;
    public dias_habiles:number;
    public estado:boolean;
    public respuesta_dias_habiles:boolean;
    public prefijo:string;
    public cantidad_conse:number;



    constructor(codigo_documento:string,fk_codigo_tipo_pqrsd:string,
        nombre_documento:string,dias_habiles:number,estado:boolean,
        respuesta_dias_habiles:boolean,prefijo:string,cantidad_conse:number){
        
        this.codigo_documento=codigo_documento;
        this.fk_codigo_tipo_pqrsd=fk_codigo_tipo_pqrsd;
        this.nombre_documento=nombre_documento;
        this.dias_habiles=dias_habiles;
        this.estado=estado;
        this.respuesta_dias_habiles=respuesta_dias_habiles;
        this.prefijo=prefijo;
        this.cantidad_conse=cantidad_conse;
    }
}