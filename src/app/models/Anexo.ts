export class Anexo {

	codigo_anexo_recibido:string;
	//El Filename
	nombre?:string;
	//El uploadDateTime
    id_mongo:string;
    estado:boolean;
    fk_codigo_oficio_recibido?:any;

    constructor(codigo_anexo_recibido: any, 
        nombre: string,
        id_mongo: string,
        estado:boolean) 
    {
        this.nombre = nombre;
        this.id_mongo = id_mongo;
        this.estado=estado;
    }
}
