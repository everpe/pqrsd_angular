export class Tercero {
    
    // public  codigo_tercero:number;
    public  identificacion:any;
    public  tipo_identificacion:number;
	public  p_nombre:string;
	public  s_nombre?:string;
	public  p_apellido:string;
	public  s_apellido?:string;
    public  estado:boolean;
    public  email?:string;

    constructor(identificacion:any,tipo_identificacion:number,
        p_nombre:string,s_nombre:string,p_apellido:string,
        s_apellido:string,estado:boolean, email:string){
        
        // this.codigo_tercero=codigo_tercero;
        this.identificacion=identificacion;
        this.tipo_identificacion=tipo_identificacion;
        this.p_nombre=p_nombre;
        this.s_nombre=s_nombre;
        this.p_apellido=p_apellido;
        this.s_apellido=s_apellido;
        this.estado=estado;
        this.email=email;
    }
}