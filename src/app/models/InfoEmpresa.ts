export class InfoEmpresa {
  
    public codigo_informacion_empresa:string;
	public municipio:string;
	public pais:string;
	public departamento:string;
	public banderaD:string;
	public escudoD:string;
	public direccion:string;
	public correo_electronico_entidad:string;
	public numero_celular:string;
	public telefono_1?:string;
	public telefono_2?:string; 
	public fax?:string;
	public pie_pagina:string;//Slogan
	public paginaWeb:string;
    //Campos extras
    public idMongoPoliticas:string;
    public contrasena_correo:string;
    public descripcionFooterEmpresa?:string;

    constructor(
        codigo_informacion_empresa: string, 
        pais:string,
        departamento:string,
        municipio:string,
        banderaD:string,
        escudoD:string,
        direccion:string,
        correo_electronico_entidad:string,
        numero_celular:string, 
        telefono_1:string,
        telefono_2: string,
        fax:string, 
        pie_pagina:string,
        paginaWeb:string,
        //Extras Fields
        idMongoPoliticas:string,
        contrasena_correo?:string)
    { 
        this.codigo_informacion_empresa=codigo_informacion_empresa;  
        this.pais=pais;
        this.departamento=departamento;
        this.municipio=municipio;
        this.banderaD=banderaD;
        this.escudoD=escudoD;
        this.direccion=direccion;
        this.correo_electronico_entidad=correo_electronico_entidad;
        this.numero_celular=numero_celular;
        this.telefono_1=telefono_1;
        this.telefono_2=telefono_2;
        this.fax=fax;
        this.pie_pagina=pie_pagina;
        this.paginaWeb=paginaWeb;
        this.idMongoPoliticas=idMongoPoliticas;
        this.contrasena_correo=contrasena_correo;
    }
}