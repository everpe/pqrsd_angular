import { Component, OnInit } from '@angular/core';
import {  HttpEventType, HttpResponse} from '@angular/common/http';
import {AnexoService} from '../../services/anexo.service';
import { Observable } from 'rxjs';
import {ProgressBarMode} from '@angular/material/progress-bar';
import {Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {


  mode: ProgressBarMode = 'indeterminate';
  selectedFiles: FileList;
  enviarFiles: File[]=[];//de los archivos que seleccione solo guarda lo validos
  progressInfos = [];
  message = '';

  fileInfos: Observable<any>;
  valido: boolean = true;
  captcha : boolean = false;

  //llave google para el captcha.
  siteKey:string;

  @Output()propagar = new EventEmitter<File[]>();
  // @Output()propagar = new EventEmitter<FileList>();
  mensaje: string;

  //arreglo contador para el numero de inputFile que se va a tener
  contador:number[]=[];
  cont:number=0;//para q no se repita lo que se agrega en contador y se pueda sacar la posicion en la iteracion
  
  constructor(
    private fileService: AnexoService,
    private toastr: ToastrService) { 
    this.siteKey='6LevMdwZAAAAAFXkqh_Zv9AwZcalchoTOOjuGkS_';
    this.valido=true;
  }

  ngOnInit(): void {
    // this.fileInfos = this.fileService.listFiles();
  }

  /**
   * Cuando se completa el captcha bien se ejecuta este  metodo,se activa el boton o form. 
   * @param event 
   */
  handleSuccessCaptcha(){
    // console.log('si fonciona');
    this.captcha=true;
  }



  /**
   * Valida cantidad de archivos seleccionados y valida los 3Mb.
   * Luego manda a guardar pintar(guardar en Mongo).
   */
  /*
  uploadFiles() {
    this.message = '';

    if(this.selectedFiles.length <= 3){

      for (let i = 0; i < this.selectedFiles.length; i++) {

        console.log('tamaño'+this.selectedFiles[i].size);
        if(this.selectedFiles[i].size>300000000){
          this.valido=false;       
        }
        this.upload(i, this.selectedFiles[i]); 
        // else{
        //   console.log("tamaño"+this.selectedFiles[i].size);
        //   this.message = 'Error El archivo: ' + this.selectedFiles[i].name + " supera las 3Mb" ;
        //   this.valido= false; 
        // }
      }
      this.onPropagar();
    }else{
      this.message = 'Ha excedido el número de archivos permitidos'; 
      this.valido=false; 
    }
   
  }

  upload(idx, file) {
    this.progressInfos[idx] = { value: 0, fileName: file.name, tamano : file.size, tipo : file.type };
    console.log(file.type);
    if(this.valido){
      
      //a este punto ya valido tamaño y cantidad
      if(file.type=='application/pdf'||file.type=='image/jpeg'||file.type=='image/jpg'){
        this.progressInfos[idx].value = 100;
        this.enviarFiles[idx]=this.selectedFiles[idx];
        this.valido=true;
      }else{
        this.message = 'Error Extensión de archivo Inválida ';
        this.valido=false;
        
      }
    /* subia de una a la bd se cambio para que se suva desde createPqrsd 
    this.fileService.pushFileToStorage(file).subscribe(
        response => {
          this.progressInfos[idx].value = 100;//Math.round(100 * event.loaded / event.total);
          },
        error => {
          this.progressInfos[idx].value = 0;
          this.message = 'No se pudo subir el archivo: ' + file.name;
      });
    *
    }
    else{
        this.message = 'Error El archivo: ' + this.selectedFiles[idx].name + " supera las 3Mb" ;
        this.valido= false; 
      }
  }
  */

/**
 * Revisa que no haya indefinodos en el arreglo enviarFiles
 */
 checkFilesBeforeSend(){
  for (let j = 0; j < this.enviarFiles.length; j++) {
    
    if(this.enviarFiles[j]===undefined){
      console.log('cccct'+this.enviarFiles[j]); 
      this.enviarFiles.slice(j,1);
      console.log('tamano quedo:'+this.enviarFiles.length)
    }
  }
 }


  //envia los archivos al componente de CreatePqrsd
  onPropagar() {
    // this.checkFilesBeforeSend();
    this.propagar.emit(this.enviarFiles);

  }





  //agrega un input File
  agregarAnexo(){
    if(this.contador.length<=2){
      
      // this.enviarFiles.push(undefined);
      this.contador.push(this.cont);
      this.cont++;
      // this.con=this.contador.length;
      // console.log('AgregaGrafico::  '+this.enviarFiles.length);
    }else{
      this.enviarFiles=[];
    }
  }


  
  /**
   * obtener los archivos seleccionados que vamos a cargar.
   * @param event el file
   */
  selectFiles(event,item) {
  
    this.selectedFiles=event.target.files;
    if(this.selectedFiles[0]!=undefined&&this.selectedFiles[0]!=null
                                &&this.selectedFiles[0].size<=3000000){
 
      // let pos = this.enviarFiles.indexOf(item);                     
      // console.log('a la posicion'+pos);                    
      // this.enviarFiles.push(this.selectedFiles[0]);
      this.enviarFiles[item]=this.selectedFiles[0];//hacer un metodo antes del propagar que valide si hay indefinido y si hay lo elimine y ahi si lo envia al  create

      this.onPropagar();
    }else{
      this.toastr.error('Ha excedido el tamaño Máximo permitido por archivo (3Mb)','',{
        progressBar:true,
        timeOut: 10000
      });
    }
    
  }




  /**
   * Elimina un anexo del arreglo de anexos que se envia
   */
  deleteAnexo(item){
    //posicion,eliminaciones 
    
    let pos = this.contador.indexOf(item); 
    //borra lo que haya en esa posicion
    this.contador.splice(pos,1);
    this.enviarFiles.splice(pos,1);

    this.cont--;
    this.onPropagar();

    // console.log('enviarTamaño'+this.enviarFiles.length);

  }

}
