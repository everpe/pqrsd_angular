import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
@Component({
  selector: 'ventana-creacion-pqrsd',
  templateUrl: './ventana-creacion-pqrsd.component.html',
  styleUrls: ['./ventana-creacion-pqrsd.component.css']
})
export class VentanaCreacionPqrsdComponent implements OnInit {
  vari:string;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }
  //atributo para la descarga del Qr local
  href : any;

  /**
   * Forma de descargar no recomendada con indice del Tag img.
   */
  downloadImage() {
    let length =  document.getElementsByTagName('img').length;
    this.href = document.getElementsByTagName('img')[length-1].src;
  }



  /**
   * Manda a guardar la imagen del QR code
   * @param parent, id de la imagen Qr renderizada por la libreria an_qrcode 
   */
  saveAsImage(parent) {
    //el result es la base 64
    const parentElement = parent.result;
    // converts base 64 encoded image to blobData
    let blobData = this.convertBase64ToBlob(parentElement);
    // Guarda como Imagen el blob Obtenido de la base 64
    if (window.navigator && window.navigator.msSaveOrOpenBlob) { //IE
      window.navigator.msSaveOrOpenBlob(blobData, 'Qrcode');
    } else { // chrome
      const blob = new Blob([blobData], { type: "image/png" });
      const url = window.URL.createObjectURL(blob);
      // window.open(url);
      const link = document.createElement('a');
      link.href = url;
      link.download = 'CodigoQr';
      link.click();
    }
  }

  /**
   * Convierte la base64 de una imagen y la convierte el blob para descargarla luego
   * @param Base64Image, la base64 de la imagen a descargar 
   */
  private convertBase64ToBlob(Base64Image: any) {
    // SPLIT INTO TWO PARTS
    const parts = Base64Image.split(';base64,');
    // HOLD THE CONTENT TYPE
    const imageType = parts[0].split(':')[1];
    // DECODE BASE64 STRING
    const decodedData = window.atob(parts[1]);
    // CREATE UNIT8ARRAY OF SIZE SAME AS ROW DATA LENGTH
    const uInt8Array = new Uint8Array(decodedData.length);
    // INSERT ALL CHARACTER CODE INTO UINT8ARRAY
    for (let i = 0; i < decodedData.length; ++i) {
      uInt8Array[i] = decodedData.charCodeAt(i);
    }
    // RETURN BLOB IMAGE AFTER CONVERSION
    return new Blob([uInt8Array], { type: imageType });
  }

  
}
