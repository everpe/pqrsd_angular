import { Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import {RegisterTerceroComponent} from '../../components/register-tercero/register-tercero.component';
import {Pqrsd} from '../../models/Pqrsd';
import {Tercero} from '../../models/Tercero';
import {Anexo} from '../../models/Anexo';
import {TipoDocumento} from '../../models/TipoDocumento'
import {TerceroService} from '../../services/tercero.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import {formatDate} from '@angular/common';
import {PqrsdService} from '../../services/pqrsd.service'
import {TipoIdentificacionService} from '../../services/tipo-identificacion.service'
import {TipoIdentificacion} from '../../models/TipoIdentificacion';
import {VentanaCreacionPqrsdComponent} from '../ventana-creacion-pqrsd/ventana-creacion-pqrsd.component';
import {AnexoService} from '../../services/anexo.service';
import {TipoDocumentoService} from '../../services/tipo-documento.service';
import { NgxSpinnerService } from "ngx-spinner";
import {InfoEmpresaService} from '../../services/info-empresa.service'
import {InfoEmpresa} from '../../models/InfoEmpresa';


@Component({
  selector: 'create-pqrsd',
  templateUrl: './create-pqrsd.component.html',
  styleUrls: ['./create-pqrsd.component.css']
})
export class CreatePqrsdComponent implements OnInit {

  //tipo de pqrsd que se creara, viene de routerLink
  public nombreTipoDoc:string;
  public idTipoPqrsd:string;
  public idTipoDocumento:string;
  //Variantes booleanas
  public denunciaAnonima:boolean;
  public autorizaCorreo:boolean;
  public adjuntaAnexos:boolean=false;
  recibidosFiles: File[]=[];//los anexos que se traen de de FileUploadComponent
  //El pqrsd que se va a crear.
  public pqrsd:Pqrsd;
  //El tercero que crea la pqr
  public tercero:Tercero;
  //fecha de Pqr
  myDate:any = new Date();
  //Lista de los tipos de identificacion.
  tiposI:TipoIdentificacion[]=[];
  tipoIdentSeleccionado:TipoIdentificacion=new TipoIdentificacion(null,null,false,false);
  //Lista de los tipos de Documento.
  tiposDocumento:TipoDocumento[]=[];
  public tipoDocumentoSeleccionado:TipoDocumento= new TipoDocumento(null,null,'',null,true,true,null,null);
  public tipoDoc:boolean=false;//auxiliar para validar el boton crearDisable
  //para que no habilte boton crear, sino encontro el tercero.
  public identificado:boolean;

  //acepta o no acepta tratamineto de Datos
  public tratamientoDatos:boolean;
  public empresa : InfoEmpresa;//para mostrar el nombre de la entidad en TratamientoDatos


  //Para visualizar el pdf de tratamiento de Datos.
  @ViewChild('pdfViewerOnDemand') pdfViewerOnDemand;
  @ViewChild('pdfViewerAutoLoad') pdfViewerAutoLoad;
  constructor( private route: ActivatedRoute,
              private dialog: MatDialog,
              private toastr: ToastrService,
              private pqrsdService:PqrsdService,
              private tipoIdentificacionService: TipoIdentificacionService ,
              private terceroService: TerceroService,
              private anexoService: AnexoService,
              private spinner: NgxSpinnerService,
              private tipoDocService:TipoDocumentoService,
              private empresaService: InfoEmpresaService) 
    { 
    //para cuando se identifica un tercero por #identificacion            
    this.identificado=false;                  
    this.denunciaAnonima=false;   
    this.autorizaCorreo=true;         
    this.setParameterRouterLink();            
    this.myDate = formatDate(new Date() , 'yyyy/MM/dd', 'en');      
    this.pqrsd= new Pqrsd(this.myDate,'',this.myDate,true,
                          false,false,'',this.tipoDocumentoSeleccionado);
    this.tercero= new Tercero(null,null,'','','','',false,'');
    this.listTiposDocumentoFromPqrsd();
  } 

  ngOnInit(): void {
      this.empresaService.listFiles().subscribe(
        response => {
          this.empresa = response;
        },
        err => {
          console.log(err);
        }
      );
  }

  /**
   * Define el nombre del tipo de Documento y el id para crear Pqrsd.
   */
  setParameterRouterLink():void{
    //Para darle el tipo de pqr que viene derouterLink 
    this.route.queryParams
      .subscribe(params => {
        this.nombreTipoDoc = params.tipoDocumento;
        this.idTipoPqrsd=params.idTipoPqrsd;
        // this.idTipoDoc=params.idTipoDoc;
      });
    this.listarTiposI();
    this.listTiposDocumentoFromPqrsd();
  }


  

  /**
   * Abre el dialogo que contiene el RegisterTercero y  extrae el tercero.
   */
  openModal():void{
    const dialogRef = this.dialog.open(RegisterTerceroComponent, {
      width: 'auto',height:'auto'// data:{status:this.r,message:this.message}
    });
    dialogRef.disableClose = true;
    //Evento cuando se cierra el dialogo de RegisterTercero
    dialogRef.afterClosed().subscribe(result => {
      //  console.log(`Dialog result: ${result.tercero.tipo_identificacion}`);
      if(result!=null && result!=undefined && result!=""){

        this.tercero=result.tercero;
        this.identificado=true;
        this.setTipoIdentificacion(this.tercero.tipo_identificacion);

        if(result.message=="Ya se encuentra registrado"){
          this.toastr.info(result.message,result.tercero.p_nombre,{
            progressBar:true,timeOut: 10000
          });
        }else if (result.message=="No se ha podido registrar"){
          this.toastr.error(result.message,'',{
            progressBar:true,timeOut: 10000
          });
        }else if(result.message=="Registrado con éxito"){
          this.toastr.success(result.message,result.tercero.p_nombre,{
            progressBar:true,timeOut: 10000
          });
        }
        //Llena el correo con el Tercero que traiga el Register
        if(result.tercero !== null && result.tercero !== undefined 
          && result.tercero.email !== undefined && result.tercero.email !== null){
          this.pqrsd.correo_envio=result.tercero.email;
        }else{
          this.pqrsd.correo_envio="";
        }

      }
     
    });
  }

  /**
   * Define si va a haber denuncia Anonima. 
   */
  setAnonimo(){
    this.denunciaAnonima=!this.denunciaAnonima;
    if(this.denunciaAnonima)
      this.tercero= this.tercero= new Tercero(null,null,'','','','',true,'');
  }

  /**
   * Define si autoriza respuesta del pqr por correo.
   */
  setAutorizaCorreo(){
    this.autorizaCorreo=!this.autorizaCorreo;
    this.pqrsd.autorizacion_correo=this.autorizaCorreo;
  }



  setAdjuntaAnexos(){
    this.adjuntaAnexos=!this.adjuntaAnexos;
  }
    /**
   * LLena el comboBox con los tipoIdentificacion
   */
  listarTiposI(): void {
    this.tipoIdentificacionService.listFiles().subscribe(
      data => {this.tiposI = data;},
      err => {console.log(err);}
    );
  }

  /**
   * busca el TipoIdentificacion por el nombre seleccionado en el combo o FindTercero o RegisterTercero.
   */
  setTipoIdentificacion(event:any){
    if(typeof event === 'number'){//si es numero es por que se llama cuando le da enter en tipoIdentidentificacion
      this.tipoIdentificacionService.findTipoById(event).subscribe(
        response=>{
          this.tipoIdentSeleccionado=response;
          this.identificado=true;
          // console.log(JSON.stringify(this.tipoIdentSeleccionado));
        },
        error=>{
          console.log(error);
          this.identificado=false;
          this.tipoIdentSeleccionado=new TipoIdentificacion(null,null,false,false);
        }
      );
    }
    
    
  }

  /**
   * Evento Boton crear Pqrsd,en find() si encuentra Tercero manda a crear el pqrsd
   * @param form, el form que se transforma en Pqrsd(binding)
   */
  crearPqrsd(form){
    this.findTercero(form);
  }

  /**
   * Busca el tercero y lo agrega o muestra que no lo encontro.
   * @param formPqrsd, si recibe este parametro es porque se esta llamando éste metodo
   *  desde createPqrsd() cuando se habilita el boton Crear, si llega nulo es porque se
   *  esta llamando desde los eventos para buscarTercero
   */
   findTercero(formPqrsd){
    if(!this.denunciaAnonima&&this.tercero.identificacion!=null){
      this.showSpinner(1500);  
      this.terceroService.findTercero(this.tercero.identificacion).subscribe(
        response=>{
          if(response!=null && response!=undefined){;
            this.tercero=response;
            this.identificado=true;
            this.setTipoIdentificacion(this.tercero.tipo_identificacion);
            this.pqrsd.correo_envio=this.tercero.email;
          }else{
            this.identificado=false;
            this.toastr.warning(this.tercero.identificacion,'No se encuestra registrado ese número de identificación: ',{
                progressBar:true,timeOut: 10000});
            //EQUIVALE A LIMPIAR EL FORM, CUANDO NO ENCONTRÓ UN TERCERO 
            this.tercero= new Tercero(null,null,'','','','',false,'');
            this.pqrsd.correo_envio="";
            this.tipoIdentSeleccionado= new  TipoIdentificacion(null,null,false,false);
          }
          // !!Manda a guardar PQRSD si es que recibe el Tercero,
          // En el momento en que ha recibo el Tercero(response)
          if(formPqrsd!=null){
            // console.log('mando guardar desdeFind Identificado')
            this.savePqrsd(formPqrsd);
          }
            
        },error=>{console.log(<any>error);
          this.identificado=false;
        });
    }
    //Si no es anónima no necesita esperar response de Tercero para mandar guardarPqrsd
    else{
      //lo manda a guardar sabiendo que no esta identificado y no necesita subscribe
      if(formPqrsd!=null){
        // console.log('mando guardar desdeFind Anonimo')
        this.savePqrsd(formPqrsd);
      }
    }
  }
  
   /**
    * Guarda el Pqrsd, con el Tercero o anónimo si selecciona el Check
    */
  savePqrsd(form){
    this.setConfig();
    this.showSpinner(2500);
    //Crear un Pqrsd con Tercero Identificado
    if(this.tercero.identificacion!=null && this.identificado ){
      // console.log('save() envioNormal: ', JSON.stringify(this.pqrsd.tipoDocumento));
      this.pqrsdService.createPqrsd(this.pqrsd,this.tercero.identificacion,this.tipoDocumentoSeleccionado.codigo_documento).subscribe(
        response=>{
          this.pqrsd=response;
          if(this.adjuntaAnexos&&this.recibirAnexos.length>0){
            this.almacenarAnexos();    
          }   
          //vista de creacion del Pqrsd con su radicado y QR
          this.openVentanaCreacion();
        },
        error=>{
          this.toastr.info('Por Favor Intente Nuevamente','Los Sentimos no se pudó Crear el Oficio:',{
            progressBar:true,timeOut: 10000,
            positionClass: 'toast-center-center',});
          console.log(error);
        }    
      );
    }
    //Crear Denuncia Anónima, se le envia la identificacion del tercero'null'
    else if(this.tercero.identificacion==null && this.denunciaAnonima){
      // console.log('envia anonima',this.pqrsd,'null',this.tipoDocumentoSeleccionado.codigo_documento);
      this.pqrsdService.createPqrsd(this.pqrsd,'null',this.tipoDocumentoSeleccionado.codigo_documento).subscribe(
        response=>{
          // console.log('recibo anonimo',JSON.stringify(response));
          this.pqrsd=response;
          //manda crear anexos si hay
          if(this.adjuntaAnexos&&this.recibirAnexos.length>0){
            this.almacenarAnexos();    
          } 
          //vista de creacion del Pqrsd con su radicado y QR
          this.openVentanaCreacion();
        },
        error=>{
          this.toastr.info('Por Favor Intente Nuevamente','Los Sentimos no se pudó Crear el Oficio:',{
            progressBar:true,timeOut: 10000,
            positionClass: 'toast-center-center',});
          console.log(error);
        }    
      );
    }
  }

  /**
   * Muestra el Spinner de carga en alguna accion
   */
  showSpinner(duracion){
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    },duracion);
  }

  /**
   * Abre un dialogo con la venta de Creación del Pqrsd.
   * Le envia los parametros data a esa vista pa radicado... 
   */
  openVentanaCreacion(){
    const dialogRef = this.dialog.open(VentanaCreacionPqrsdComponent,{
      width: '60%',
      data:{tipo:this.nombreTipoDoc ,radicado:this.pqrsd.numero_radicado,
      codigo_qr:this.pqrsd.codigo_qr}
    });
    dialogRef.afterClosed().subscribe(
      result => {window.location.reload();}
    );
    dialogRef.disableClose = true;
  }

  /**
   * Configura las fechas para el formato qu eusa Java y otros
   */
  setConfig(){
    if(!this.autorizaCorreo){
      this.pqrsd.correo_envio=null;
    }
    this.pqrsd.autorizacion_correo=this.autorizaCorreo;
    this.pqrsd.fecha_oficio=this.pqrsd.fecha_oficio.replaceAll('/','-');
    this.pqrsd.fecha_radicado=this.pqrsd.fecha_radicado.replaceAll('/','-');
  }

/**
 * Recibe los anexos del UploadFileComponent 
 * @param anexos 
 */
  recibirAnexos(anexos:File[]) {
    this.recibidosFiles=anexos;
  }

  /**
   * Manda a guardar los files a mongo
   */
  almacenarAnexos(){
    for (let i = 0; i < this.recibidosFiles.length; i++) {
      //if para cunado envia vacio el file
      if(this.recibidosFiles[i]!== null && this.recibidosFiles[i]!==undefined){
        // console.log('manda guardar:'+this.recibidosFiles[i].name);
        this.anexoService.pushFileToStorage(this.recibidosFiles[i]).subscribe(
        response => {
          // Cuando guarda el FileMongo manda crear anexo Entidad
          let id_Mongo:any=response;
          // console.log('el idMongo es '+id_Mongo.idMongo);
          let anexo  :Anexo = new Anexo(null,this.recibidosFiles[i].name,id_Mongo.idMongo,true);
          anexo.fk_codigo_oficio_recibido=this.pqrsd.codigo_oficio_recibido;
          //Manda a crear en la entidad anexo
          this.anexoService.createAnexo(anexo).subscribe(
            response=>{
              // console.log(response);
            },
            error=>{
              console.log(error);
            }
          );
        },
        error => {
          console.log(error);
        });
      }
      

    }
  }



  /**
   * Consulta todos los detalle:tipo_documento que pertenecen al TipoPqrsd que se va a crear.
   */
  listTiposDocumentoFromPqrsd(){
    this.tipoDocService.findTiposByTipoPqrsd(this.idTipoPqrsd).subscribe(
      response=>{
        this.tiposDocumento=response;
      },
      error=>{
        console.log(error);
      }
    );
  }

  /**
   * Cuando selecciona el tipoDocumento a crear en el combobox.
   * @param event 
   */
  setTipoDocumento(event){
    // - Tipo Proceso -
    if(event.target.value!=""){
      // console.log('se seleccciono'+JSON.stringify(this.tipoDocumentoSeleccionado.nombre_documento));
      this.tipoDocService.findTipoDocByNombre(event.target.value,this.idTipoPqrsd).subscribe(
        response=>{
          // console.log(response);
          this.tipoDocumentoSeleccionado=response;
          this.pqrsd.tipoDocumento=this.tipoDocumentoSeleccionado;
          this.tipoDoc=true;
        },
        error=>{
          console.log(error);
          this.tipoDoc=false;
        }
      );
    }else{
      this.tipoDoc=false;
      this.tipoDocumentoSeleccionado.codigo_documento==null;
    }
    
  }

  /**
   * Activa o desactiva la política de datos
   */
  setPoliticaDatos(){
    this.tratamientoDatos=!this.tratamientoDatos;
  }

  /**
   * Obtiene el File de tratamiento de Datos y lo visualiza,Siempre debe ser PDF
   */
  getPdfTrataminetoDatos(){
    let id=this.empresa.idMongoPoliticas;
    this.anexoService.downloadFile({ 'id': id})
    .subscribe(data => {
      let a:any=new Blob([data],{type: 'application/pdf'});
      if(a!=null&&a!=undefined){
        this.pdfViewerOnDemand.pdfSrc = a; //pdfSrc can be Blob or Uint8Array
        this.pdfViewerOnDemand.refresh();
      }    
    });
  }


  /**
   * Consulta Por Evento, el Tercero cuando le da enter campo identificación. 
   */
  searchTerceroKeyEnter(){
    if(this.tercero.identificacion!=undefined && this.tercero.identificacion!=""
     && this.tercero.identificacion!=null){    
      this.findTercero(null);
    }
  }

}
