import { Component, OnInit,ViewChild  } from '@angular/core';
import {AnexoService} from '../../services/anexo.service'
import {PqrsdService} from '../../services/pqrsd.service'
import{Anexo} from '../../models/Anexo'
import {DomSanitizer,SafeUrl} from '@angular/platform-browser';
import { saveAs } from 'file-saver';
import { Pqrsd } from 'src/app/models/Pqrsd';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { CloseScrollStrategy } from '@angular/cdk/overlay';
// import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators';


const MIME_TYPES = {
  pdf: 'application/pdf',
  jpeg:'image/jpeg',
  jpg:'image/jpg',
  png:'image/png',
  PNG:'image/PNG'
}

@Component({
  selector: 'file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.css']
}) 
export class FileListComponent implements OnInit {

  radicado:string='';
  oficio:Pqrsd= new Pqrsd('','','',null,null,null,'',null);
  //los anexos del oficio.
  archivos:Anexo[]=[];
  verAnexos:boolean=false;
  nameImage:string;
  //la imagen que se pinta
  image: SafeUrl | null = null;
  @ViewChild('pdfViewerOnDemand') pdfViewerOnDemand;
  @ViewChild('pdfViewerAutoLoad') pdfViewerAutoLoad;
  constructor(private anexoService: AnexoService,
              private pqrsdService:PqrsdService,
              private toastr: ToastrService,
              private http: HttpClient,
              private sanitizer:DomSanitizer)
  { }

  ngOnInit(): void {
  }
  /**
   * LLena el objeto Pqrsd
   */
  buscarPqrsd(){
    this.pqrsdService.findOficioByRadicado(this.radicado).subscribe(
      response=>{
        if(response!=null){
          this.oficio=response;
          // console.log(this.oficio);
          this.listarFiles();
        }else{
          this.oficio=  new Pqrsd('','','',null,null,null,'',null);

          this.archivos=[];
          this.toastr.warning(this.radicado,'Los entimos, No se encontró el Oficio con radicado: ',{
            progressBar:true,
            timeOut: 10000
          });
        }
      },error=>{
        console.log(error);
        this.toastr.warning(this.radicado,' No se encontró el Oficio con radicado: ',{
          progressBar:true,
          timeOut: 10000
        });
      }        
    );
  }


  /**
   * Lista de anexos que pertenecen al OficioPqrsd
   */
  listarFiles(): void {
    if(this.oficio.numero_radicado!=null){
      this.anexoService.listAnexosRecibidos(this.oficio.codigo_oficio_recibido).subscribe(
        data => {
          this.archivos = data;
        },err => {
          console.log(err);
        }
      );
    }else{
      console.log('debe consultar un pqr antes');
    }
  }


  setVerAnexos(){
    this.verAnexos=!this.verAnexos;
  }



  downloadFile(id,filename) {
    const EXT = filename.substr(filename.lastIndexOf('.') + 1);
    this.anexoService.downloadFile({ 'id': id,'filename':filename})
    .subscribe(data => {
      let a:any=new Blob([data],{type: MIME_TYPES[EXT]});
      saveAs(new Blob([data], {type: MIME_TYPES[EXT]}),  filename);               
    });
  }


  showFileInBrowser(id,filename){
    const EXT = filename.substr(filename.lastIndexOf('.') + 1);
    this.anexoService.downloadFile({ 'id': id,'filename':filename})
    .subscribe(data => {
      let a:any=new Blob([data],{type: MIME_TYPES[EXT]});
      if(EXT=='pdf'||EXT=='PDF'){
        this.pdfViewerOnDemand.pdfSrc = a; //pdfSrc can be Blob or Uint8Array
        this.pdfViewerOnDemand.refresh();
      }else{//muestra imagen en la misma página.
        this.nameImage=filename;
        const unsafeImg = URL.createObjectURL(a);
        this.image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
        window.open(unsafeImg,'_blank', "width=auto, height=auto");
      }     
    });
  }
  

  saveAsImage(parent) {
    //el result es la base 64
    const parentElement = parent.result;
    // converts base 64 encoded image to blobData
    let blobData = this.convertBase64ToBlob(parentElement);
    // Guarda como Imagen el blob Obtenido de la base 64
    if (window.navigator && window.navigator.msSaveOrOpenBlob) { //IE
      window.navigator.msSaveOrOpenBlob(blobData, 'Qrcode');
    } else { // chrome
      const blob = new Blob([blobData], { type: "image/png" });
      const url = window.URL.createObjectURL(blob);
      // window.open(url);
      const link = document.createElement('a');
      link.href = url;
      link.download = 'CodigoQr';
      link.click();
    }
  }

  /**
   * Convierte la base64 de una imagen y la convierte el blob para descargarla luego
   * @param Base64Image, la base64 de la imagen a descargar 
   */
  private convertBase64ToBlob(Base64Image: any) {
    // SPLIT INTO TWO PARTS
    const parts = Base64Image.split(';base64,');
    // HOLD THE CONTENT TYPE
    const imageType = parts[0].split(':')[1];
    // DECODE BASE64 STRING
    const decodedData = window.atob(parts[1]);
    // CREATE UNIT8ARRAY OF SIZE SAME AS ROW DATA LENGTH
    const uInt8Array = new Uint8Array(decodedData.length);
    // INSERT ALL CHARACTER CODE INTO UINT8ARRAY
    for (let i = 0; i < decodedData.length; ++i) {
      uInt8Array[i] = decodedData.charCodeAt(i);
    }
    // RETURN BLOB IMAGE AFTER CONVERSION
    return new Blob([uInt8Array], { type: imageType });
  }

/**
 * Metodo Util para convertir de Blob a File
 * @param theBlob 
 * @param fileName 
 */
  // public blobToFile = (theBlob: Blob, fileName:string): File => {
  //   var b: any = theBlob;
  //   b.lastModifiedDate = new Date();
  //   b.name = fileName;
  //   return <File>theBlob;
  // }


}
