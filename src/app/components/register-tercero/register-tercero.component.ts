import { Component, OnInit } from '@angular/core';
import {Tercero} from '../../models/Tercero';
import {TerceroService} from '../../services/tercero.service'
import {TipoIdentificacion} from '../../models/TipoIdentificacion';
import {TipoIdentificacionService} from '../../services/tipo-identificacion.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DialogData} from '../../models/DialogData';
import { NgxSpinnerService } from "ngx-spinner";
import { Inject} from '@angular/core';
@Component({
  selector: 'app-register-tercero',
  templateUrl: './register-tercero.component.html',
  styleUrls: ['./register-tercero.component.css']
})

export class RegisterTerceroComponent implements OnInit {

  public tercero:Tercero;

  // const data:DialogData = {message:"",tercero:this.tercero};
  public data = {} as DialogData;
  //Lista de los tipos de identificacion.
  tiposI:TipoIdentificacion[]=[];
  tipoSeleccionado:TipoIdentificacion;
  //  = new TipoIdentificacion("","",false,false);

  /**
   * Recibe el DialogRef ya que se muestra a traves de Dialog
   *  en create tercero
   * @param dialogRef, instancia del dialogo que lo contiene 
   */
  constructor(private terceroService: TerceroService ,
              private tipoIdentificacionService: TipoIdentificacionService , 
              public dialogRef: MatDialogRef<RegisterTerceroComponent>,
              private spinner: NgxSpinnerService  ) { 
    this.tercero=  new Tercero('',-5,'','','','',true,'')
    this.tipoSeleccionado= new TipoIdentificacion("","",false,false);
  }

  ngOnInit(): void {
    this.listarTiposI();
  }

  /**
   * busca el TipoIdentificacion por el nombre seleccionado en el combo.
   */
  setTipo(event){
    // console.log('id'+event.target.value);
    if(event!=null&&event.target.value!=="- Tipo Identificación -"&&
    event!=null&&event.target.value!==""){
      this.tipoIdentificacionService.findTipoById(event.target.value).subscribe(
        data => {
          this.tipoSeleccionado=data;
          this.tercero.tipo_identificacion=data.id;
          // console.log(this.tipoSeleccionado);
        },
        err => {
          console.log(err);
        }
      );
      
    }else{
      // console.log('igualll');
      this.tipoSeleccionado = new TipoIdentificacion(null,null,false,false);
    }
  }

  /**
   * LLena el comboBox con los tipoIdentificacion
   */
  listarTiposI(): void {
    this.tipoIdentificacionService.listFiles().subscribe(
      data => {
        this.tiposI = data;
        // console.log(data);
      },
      err => {
        console.log(err);
      }
    );
  }



  /**
   * Al click en registrar Tercero 
   * lo consulta por identificacion sino esta intenta registrarlo
   * @param Form 
   */
  onSubmit(form){
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();},2000);
    this.terceroService.findTercero(this.tercero.identificacion).subscribe(
      response=>{
        if(response!=null){
          //se recibe y valida este mensaje en CreatePqrsd
          this.data.message="Ya se encuentra registrado";
          this.data.tercero=response;
        }else{
          this.saveTercero();
        }
        this.cerrar(this.data);
      },
      error=>{
        console.log(<any>error)
        this.data.message="No se ha podido registrar";
        this.data.tercero=null;
      }
    );

    
   
     
  }


  /**
   * Registra nuevo Tercero, solo si ha seleccionado TipoIdentificacion
   */
saveTercero(){
  if(this.tipoSeleccionado!=null&&this.tipoSeleccionado!=undefined){
    // this.data.tercero.tipo_identificacion=this.tercero.tipo_identificacion;
    this.terceroService.createTercero(this.tercero).subscribe(
      response=>{
        // console.log(response.identificacion);
        this.data.message="Registrado con éxito";
        this.data.tercero=this.tercero;      
      },
      error=>{
        console.log(<any>error)
        this.data.message="No se ha podido registrar";
        this.data.tercero=null;
      }
    );
    this.cerrar(this.data);
  }
  
}

  /**
   * Cierra el dialogo que contiene El register-tercero
   * @param creado, si se pudo registrar o no el tercero. 
   */
  cerrar(data:DialogData): void {
    this.dialogRef.close(data);
  }
  
}
