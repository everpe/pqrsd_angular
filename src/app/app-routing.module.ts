import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import  {FileListComponent }  from './components/file-list/file-list.component';
import  {FileUploadComponent }  from './components/file-upload/file-upload.component';
import  {InitialComponent }  from './components/initial/initial.component';
import {CreatePqrsdComponent} from './components/create-pqrsd/create-pqrsd.component';

const routes: Routes = [
  {path: 'initial', component: InitialComponent},
  {path: 'list/files', component: FileListComponent},
  {path: 'upload/file', component: FileUploadComponent},
  {path: 'create/pqrsd', component: CreatePqrsdComponent},
  { path: '',   redirectTo: '/initial', pathMatch: 'full' }
  // {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], 
  exports: [RouterModule]
})
export class AppRoutingModule { }