import { Injectable } from '@angular/core';
import { HttpClient,HttpEvent, HttpRequest,HttpHeaders,HttpResponse,HttpParams   } from '@angular/common/http';
import { Observable } from 'rxjs';
import { InfoEmpresa } from '../models/InfoEmpresa';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class InfoEmpresaService {

  apiURL = '';  
  constructor(private httpClient: HttpClient) {
    this.apiURL=environment.api_url; 
   }

   /**
    * Toda la información de la entidad(Alcaldía)
    */
  public listFiles(): Observable<InfoEmpresa> {
    return this.httpClient.get<InfoEmpresa>(this.apiURL + `info/empresa`);
  }

}
