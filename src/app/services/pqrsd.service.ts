import { Injectable } from '@angular/core';
import { HttpClient,HttpEvent, HttpRequest,HttpHeaders,HttpResponse,HttpParams   } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Pqrsd} from '../models/Pqrsd'; 
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})

export class PqrsdService {

  apiURL = '';  
  constructor(private httpClient: HttpClient) { 
    this.apiURL=environment.api_url; 
  }

  /**
   * Encuentra un Oficio por su Radicado
   * @param raficado 
   */
  public findOficioByRadicado(radicado:string): Observable<Pqrsd> {
    return this.httpClient.get<Pqrsd>(this.apiURL+'find/recibido/byRadicado/'+ `${radicado}`);
  }

  /**
   * Crea un Pqrsd.
   * @param el oficio que se va a crear en bd. 
   */
  // public createPqrsd(oficio:Pqrsd,idenCreador:string): Observable<Pqrsd> {
  //     return this.httpClient.post<Pqrsd>(this.apiURL+'create/recibido/by/'+ `${idenCreador}`,oficio);
  // }
  public createPqrsd(oficio:Pqrsd,idenCreador:string,idTipoDocumento:any): Observable<Pqrsd> {
    return this.httpClient.post<Pqrsd>(this.apiURL+'create/recibido/by/'+ `${idenCreador}/`+ `${idTipoDocumento}`,oficio);
  }



  
}
