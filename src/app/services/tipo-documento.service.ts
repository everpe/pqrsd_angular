import { Injectable } from '@angular/core';
import { HttpClient,HttpEvent, HttpRequest,HttpHeaders,HttpResponse,HttpParams   } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TipoDocumento } from '../models/TipoDocumento';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipoDocumentoService {

  apiURL = '';  

  constructor(private httpClient: HttpClient) { 
    this.apiURL=environment.api_url;
  }

  /**
   * Todos los tipos_Documento.
   */
  public listAllDocumentos(): Observable<TipoDocumento[]> {
    return this.httpClient.get<TipoDocumento[]>(this.apiURL + `find/tipos/documentos`);
  }

  /**
   * Obtiene todos tipo:documento por el tipoPqrsd al que pertenecen.
   * @param id del Pqrsd al que pertenecen.
   */
  public findTiposByTipoPqrsd(idPqrsd:string):Observable<TipoDocumento[]>{
    return this.httpClient.get<TipoDocumento[]>(this.apiURL + `find/tipos/documento/by/tipo/pqrsd/`+ `${idPqrsd}`);
  }


  public findTipoDocByNombre(nombre:string,idPqrsd:any):Observable<TipoDocumento>{
    // console.log("en el servicio"+`find/tipo/documento/by/nombre/and/pqrsd/`+ `${nombre}/`+ `${idPqrsd}`);
    return this.httpClient.get<TipoDocumento>(this.apiURL + `find/tipo/documento/by/nombre/and/pqrsd/`+ `${nombre}/`+ `${idPqrsd}`);
  }


}
