import { Injectable } from '@angular/core';
import { HttpClient,HttpEvent, HttpRequest,HttpHeaders,HttpResponse,HttpParams   } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { Anexo } from '../models/fileMongo';
import { Anexo } from '../models/Anexo';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})

/**
 * Toda la gestion para archivos con GridFs
 */
export class AnexoService { 
  
  apiURL = '';  

  constructor(private httpClient: HttpClient) { 
    this.apiURL=environment.api_url; 
  }

  /**
   * Lista de los anexos que pertenecen a cierto Oficio.
   */
  public listAnexosRecibidos(codigo_oficio): Observable<Anexo[]> {
    return this.httpClient.get<Anexo[]>(this.apiURL + `anexos/recibidos/byOficio`+`/${codigo_oficio}`);
  }
  
  public createAnexo(anexo:Anexo): Observable<Anexo> {
    return this.httpClient.post<Anexo>(this.apiURL+'create/anexo/recibido',anexo);
  }







  /**
   * Obtiene todos los Files que coinciden con metadata.
   */
  public listFiles(): Observable<Anexo[]> {
    return this.httpClient.get<Anexo[]>(this.apiURL + `files/user/anonymus`);
  }


  /**
   * 
   * @param id Actualiza el nombre de un File
   * @param fileMongo 
   */
  public updateFilename(id: number, fileMongo: Anexo): Observable<any> {
    return this.httpClient.put<any>(this.apiURL + `update/${id}`, fileMongo);
  }




/**
 * Sube el File a mongo db.
 * @param file 
 */
  public pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
    const data: FormData = new FormData();
    data.append('file', file);
    console.log(data);
    return this.httpClient.post<any>(this.apiURL+'upload/file',data);
  }




/**
 * Petición para recibir un archivo de la bd y descargarlo en el pc.
 * @param data, el id y el filename del archivo. 
 */
  downloadFile(data) {
    // we would call the spring-boot service
    // let fileName=data.filename.replace(/,/g, " ");
    const REQUEST_PARAMS = new HttpParams()
    .set('id', data.id)
    // .set('filename',data.filename);
    return this.httpClient.get(this.apiURL + "download/", {
      params: REQUEST_PARAMS,
      responseType: 'arraybuffer'
    })
  }
  /*
  jquery peticion
  public retrive(id):void{
    let url=this.productoURL + 'retrive/file/${id}';
   
    fetch(url)
    .then(function(response){
      console.log(response);
    })
  }
*/
}
 