import { Injectable } from '@angular/core';
import { HttpClient,HttpEvent, HttpRequest,HttpHeaders,HttpResponse,HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
/**
 * Servicio para leer el Archivo Json con la ip
 */
export class ReadJsonFileService {
  json:any={};
  constructor(private httpClient: HttpClient) { 
    
    this.json=this.httpClient.get('../../assets/jsons/postgres.json').subscribe(
      data=>{
        console.log(data);
        this.json=data;
      }
    );
    

  }


  
}
