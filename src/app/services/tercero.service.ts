import { Injectable,EventEmitter } from '@angular/core';
import { HttpClient,HttpEvent, HttpRequest,HttpHeaders,HttpResponse,HttpParams   } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Tercero} from '../models/Tercero'; 
import { environment } from '../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class TerceroService {
//variable$=EventEmitter<String>;se pueden suscribir o emmit a esta variable desde un component(ejp)

  apiURL='';
  constructor(private httpClient: HttpClient) {
    this.apiURL=environment.api_url;      
  }


  /**
   * Manda a registrar un tercero
   * @param tercero 
   */
  public createTercero(tercero:Tercero): Observable<Tercero> {
    return this.httpClient.post<Tercero>(this.apiURL+'create/tercero',tercero);
  }

  /**
   * Busca un tercero por su identificacion.
   * @param id_tercero, identificacion del tercero 
   */
  public findTercero(id_tercero:any): Observable<any> {
    return this.httpClient.get<Tercero>(this.apiURL+'find/tercero/'+ `${id_tercero}`);
  }




}
