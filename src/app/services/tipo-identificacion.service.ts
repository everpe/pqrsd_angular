import { Injectable } from '@angular/core';
import { HttpClient,HttpEvent, HttpRequest,HttpHeaders,HttpResponse,HttpParams   } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TipoIdentificacion } from '../models/TipoIdentificacion';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class TipoIdentificacionService {

  apiURL = '';  

  constructor(private httpClient: HttpClient) { 
    this.apiURL=environment.api_url; 
  }

  /**
   * Todos los tipos de identificacion.
   */
  public listFiles(): Observable<TipoIdentificacion[]> {
    return this.httpClient.get<TipoIdentificacion[]>(this.apiURL + `all/tipo/identificacion`);
  }

  /**
   * Encuentra un TipoIdentificacion por Nombre.
   */
  public findTipoByNombre(nombre:string):Observable<TipoIdentificacion>{
    console.log(this.apiURL + `find/tipo/byNombre/`+ `${nombre}`);
    return this.httpClient.get<TipoIdentificacion>(this.apiURL + `find/tipo/byNombre/`+ `${nombre}`);
  }

  /**
   * Encuentra un TipoIdentificacion por id.
   */
  public findTipoById(id:number):Observable<TipoIdentificacion>{
    return this.httpClient.get<TipoIdentificacion>(this.apiURL + `find/tipo/byId/`+ `${id}`);
  }

}
