import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
 
//Http
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
//Toaster Y anomations library
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
//components
import { FileListComponent } from './components/file-list/file-list.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { InitialComponent } from './components/initial/initial.component';
import { PipePipe } from './pipes/pipe.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
// import  {  RecaptchaModule ,  RecaptchaFormsModule  }  from  'ng-recaptcha' 
import { CreatePqrsdComponent } from './components/create-pqrsd/create-pqrsd.component';

//AngularMaterial
import { MatSliderModule } from '@angular/material/slider';
import {MatDialogModule} from '@angular/material/dialog';
import { RegisterTerceroComponent } from './components/register-tercero/register-tercero.component'; 
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { VentanaCreacionPqrsdComponent } from './components/ventana-creacion-pqrsd/ventana-creacion-pqrsd.component';
// Spinner
import { NgxSpinnerModule } from "ngx-spinner";
//Ver Pdfs
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';
//generar Qr
import { AnQrcodeModule } from 'an-qrcode';

@NgModule({
  declarations: [
    AppComponent,
    FileListComponent,
    FileUploadComponent,
    InitialComponent,
    PipePipe,
    CreatePqrsdComponent,
    RegisterTerceroComponent,
    VentanaCreacionPqrsdComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    // RecaptchaModule ,   // este es el módulo principal de recaptcha
    // RecaptchaFormsModule ,//en caso de validar Form del captcha
    MatSliderModule,
    MatDialogModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatInputModule,
    NgxSpinnerModule,
    PdfViewerModule,
    PdfJsViewerModule,
    AnQrcodeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})    
export class AppModule { }
