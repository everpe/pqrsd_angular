import { Component } from '@angular/core';
import {InfoEmpresaService} from './services/info-empresa.service'
import {InfoEmpresa} from './models/InfoEmpresa';
import {ReadJsonFileService} from './services/read-json-file.service'

//EL ARCHIVO DE CONFIGURACION CREADO
// import listadePostres from 'src/assets/jsons/postgres.json';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'PQRSD';
  public empresa : InfoEmpresa; //= new InfoEmpresa('','','','','','','','','','','','','','');
  constructor(private empresaService: InfoEmpresaService ) {
     this.empresa = new InfoEmpresa('','','','','','','','','','','','','','','','');
    this.initEmpresa();
  }


  // ngOnInit(): void {
  //   this.initEmpresa();
  // }
  


  initEmpresa(){
    this.empresaService.listFiles().subscribe(
      response => {
        this.empresa = response;
        // console.log(this.empresa);
        // console.log('gg'+this.empresa.idMongoPoliticas);
        // console.log(response);
      },
      err => {
        console.log(err);
      }
    );
   }

}
